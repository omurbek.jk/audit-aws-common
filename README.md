audit-aws-common
============================


## Description
This repo is designed to work with CloudCoreo. This composite contains common definitions for AWS Audit composites and is not intended to be used directly.


## Hierarchy
![composite inheritance hierarchy](https://raw.githubusercontent.com/CloudCoreo/audit-aws-common/master/images/hierarchy.png "composite inheritance hierarchy")



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

**None**


## Optional variables with no default

**None**

## Tags
1. Audit
1. Best Practices
1. Alert

## Categories
1. Audit



## Diagram
![diagram](https://raw.githubusercontent.com/CloudCoreo/audit-aws-common/master/images/diagram.png "diagram")


## Icon


